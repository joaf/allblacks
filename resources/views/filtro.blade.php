<fieldset>
    <legend>Filtro</legend>
    <form id="form-filtro" action="{{'users'}}" type="post">
        <div class="col-md-12">
            <div class="row form-group">
                <div class="col-md-3">
                    <label for="endereco">Nome</label>
                    <input class="form-control" name="no_usuario" placeholder="Nome" type="text"/>
                </div>
                <div class="col-md-2">
                    <label for="cep">Documento</label>
                    <input class="form-control" name="nu_documento" placeholder="Documento" type="text"/>
                </div>
                <div class="col-md-2">
                    <label for="cep">Email</label>
                    <input class="form-control" name="email" placeholder="Email" type="text"/>
                </div>
                <div class="col-md-2">
                    <label for="uf">UF</label>
                    <select class="form-control" name="uf">
                        <option value="">Selecione</option>
                        @foreach($states as $state)
                            <option value="{{$state->id}}">{{$state->uf}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <label for="city_id">Cidade</label>
                    <select name="city_id" class="form-control" disabled>
                        <option value="">Selecione</option>
                    </select>
                </div>
            </div>
            <div class="d-flex justify-content-end">
                <div class="col-md-2 float-right">
                    <button type="submit" class="btn btn-primary float-right btn-block">Filtrar</button>
                </div>
                <div class="col-md-1 float-right">
                    <button type="reset" class="btn btn-secondary float-right">Limpar</button>
                </div>
            </div>
        </div>
    </form>
</fieldset>