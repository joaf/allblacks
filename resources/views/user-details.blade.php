<div class="loading"></div>
<fieldset>
    <legend>Detalhes do usuário</legend>
    <div class="alert alert-danger" style="display: none">
        <span>Por favor, preencha todos os campos obrigatórios!</span>
    </div>
    @if(!is_object($user))
        <div class="alert alert-danger">
            <span>Não foi possível carregar as informações do usuário, por favor, tente novamente.</span>
        </div>
    @else
        <row>
            <fieldset>
                <legend>Dados pessoais</legend>
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-8 obrigatorio">
                            <label for="nome">Nome <span class="obrigatorio">*</span></label>

                            <input class="form-control form-control-sm" placeholder="Nome" name="nome" type="text"
                                   value="{{$user->no_usuario}}"
                                   required/>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-md-4 obrigatorio">
                            <label for="documento">Documento <span class="obrigatorio">*</span></label>
                            <input class="form-control form-control-sm" name="documento" placeholder="Documento"
                                   type="text"
                                   maxlength="11"
                                   value="{{$user->nu_documento}}"/>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="col-md-6 obrigatorio">
                            <label for="email" class="control-label">Email
                                <span class="obrigatorio">*</span></label>
                            <input type="email" class="form-control form-control-sm" name="email" placeholder="Email"
                                   value="{{$user->email}}"
                                   required>
                        </div>
                        <div class="col-md-3 obrigatorio">
                            <label class="control-label">Telefone <span class="obrigatorio">*</span></label>
                            <input type="text" class="form-control form-control-sm" name="telefone" placeholder="Telefone"
                                   value="{{$user->nu_telefone}}"
                            data-mask="(99)99999-9999">
                        </div>
                    </div>
                </div>
            </fieldset>
        </row>
        <row>
            <fieldset>
                <legend>Endereco</legend>
                <div class="col-md-12">
                    <div class="row form-group">
                        <div class="col-md-6 obrigatorio">
                            <label for="endereco">Endereco <span class="obrigatorio">*</span></label>
                            <input class="form-control form-control-sm" name="no_endereco" placeholder="Endereco" type="text"
                                   value="{{$user->address[0]->no_endereco}}"/>
                        </div>
                        <div class="col-md-4 obrigatorio">
                            <label for="bairro">Bairro <span class="obrigatorio">*</span></label>
                            <input class="form-control form-control-sm" name="no_bairro" placeholder="Bairro" type="text"
                                   value="{{$user->address[0]->no_bairro}}"/>
                        </div>
                        <div class="col-md-2 obrigatorio">
                            <label for="cep">CEP <span class="obrigatorio">*</span></label>
                            <input class="form-control form-control-sm" name="nu_cep" placeholder="CEP" type="text"
                                   maxlength="9"
                                   value="{{$user->address[0]->nu_cep}}"
                                   required/>
                        </div>
                        <div class="col-md-3 obrigatorio">
                            <label for="uf">UF <span class="obrigatorio">*</span></label>
                            <select name="uf" class="form-control form-control-sm">
                                <option value="">Selecione</option>
                                @foreach($states as $state)
                                    <option value="{{$state->id}}" {{$state->uf == $user->address[0]->city->state->uf? 'selected': ''}}>{{$state->uf}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-5 obrigatorio">
                            <label for="city_id">Cidade <span class="obrigatorio">*</span></label>
                            <select name="city_id" class="form-control form-control-sm disabled">
                                <option value="{{$user->address[0]->city->id}}">{{$user->address[0]->city->no_cidade}}</option>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>
        </row>
    @endif
</fieldset>
<script type="text/javascript" src="{{asset('js/jquery.mask.js')}}"></script>

