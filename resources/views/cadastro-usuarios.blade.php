@extends('home')

@section('content')
    <div class="container box">
        <form id="form-cadastro" method="post">
            <div class="loading"></div>
            <div class="col-md-12 ">
                <div class="col-md-12">Novo Cadastro</div>
                <fieldset>
                    <legend>Cadastro de usuário</legend>
                    <div class="alert alert-danger" style="display: none">
                        <span>Por favor, preencha todos os campos obrigatórios!</span>
                    </div>
                    <row>
                        <fieldset>
                            <legend>Dados pessoais</legend>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-8 obrigatorio">
                                        <label for="nome">Nome <span class="obrigatorio">*</span></label>
                                        <input class="form-control input-sm" placeholder="Nome" name="nome" type="text"
                                               required/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-4 obrigatorio">
                                        <label for="documento">Documento <span class="obrigatorio">*</span></label>
                                        <input class="form-control" name="documento" placeholder="Documento"
                                               type="text"
                                               maxlength="11"/>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-6 obrigatorio">
                                        <label for="email" class="control-label">Email
                                            <span class="obrigatorio">*</span></label>
                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                               required>
                                    </div>
                                    <div class="col-md-3 obrigatorio">
                                        <label class="control-label">Telefone <span class="obrigatorio">*</span></label>
                                        <input type="text" class="form-control telefone" name="telefone" placeholder="Telefone">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </row>
                    <row>
                        <fieldset>
                            <legend>Endereco</legend>
                            <div class="col-md-12">
                                <div class="row form-group">
                                    <div class="col-md-7 obrigatorio">
                                        <label for="endereco">Endereco <span class="obrigatorio">*</span></label>
                                        <input class="form-control" name="endereco" placeholder="Endereco" type="text"/>
                                    </div>
                                    <div class="col-md-3 obrigatorio">
                                        <label for="bairro">Bairro <span class="obrigatorio">*</span></label>
                                        <input class="form-control" name="bairro" placeholder="Bairro" type="text"/>
                                    </div>
                                    <div class="col-md-2 obrigatorio">
                                        <label for="cep">CEP <span class="obrigatorio">*</span></label>
                                        <input class="form-control" name="cep" placeholder="CEP" type="text"
                                               maxlength="9"
                                               required/>
                                    </div>
                                    <div class="col-md-2 obrigatorio">
                                        <label for="uf">UF <span class="obrigatorio">*</span></label>
                                        <select name="uf" class="form-control">
                                            <option value="">Selecione</option>
                                            @foreach($states as $states)
                                                <option value="{{$states->id}}">{{$states->uf}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-5 obrigatorio">
                                        <label for="city_id">Cidade <span class="obrigatorio">*</span></label>
                                        <select name="city_id" class="form-control" disabled>
                                            <option value="">Selecione</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </row>
                </fieldset>
            </div>
            <div class="form-group row"></div>
            <div class="form-group row offset-11">
                <div class="fieldset-group clearfix">
                    <div class="col-ms-12">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

