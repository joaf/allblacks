@extends('home')
@section('content')
    <div class="container box">
        @include('filtro')
        <div class="flex-center">
            <div class="content">
                <div>
                    <b>Importar arquivo XML</b>
                </div>
                <form id="xml">
                    <div class="loading"></div>
                    <div class="col-md-12">
                        <label class="btn btn-primary">
                            <input type="file" id="upload" class="cbtn" name="file"/>
                            Carregar XML
                        </label>
                        <label class="btn btn-success disabled">
                            <input type="button" id="submitXml" class="cbtn" name="submit" disabled/>
                            Enviar Xml
                        </label>
                    </div>
                    <span class="custom-file-upload"></span>
                </form>
            </div>
        </div>
        <row class="col-md-12 removerDados">
            <button id="removerDados" class="float-right btn btn-danger" disabled>Remover Informações</button>
        </row>
        <div class="table-responsive">
            <table id="usuarios" class="table table-striped table-bordered table-sm container">
                <thead class="thead-dark">
                <tr>
                    <th scope="col" width="20%">Nome</th>
                    <th scope="col" width="10%">Documento</th>
                    <th scope="col" width="20%">Email</th>
                    <th scope="col" width="10%">Telefone</th>
                    <th scope="col" width="33%">Endereco</th>
                    <th scope="col" width="7%">Ações</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <th scope="row">{{$user->no_usuario}}</th>
                        <td>{{$user->nu_documento}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->nu_telefone}}</td>
                        <td>{{$user->address[0]->no_endereco}}, {{$user->address[0]->no_bairro}}
                            , {{$user->address[0]->city->no_cidade}}, {{$user->address[0]->city->state->uf}}
                        </td>
                        <td>
                            <div class="d-flex justify-content-center">
                                <a href="#" data-toggle="modal" data-target="#user-details"
                                   data-username="{{$user->no_usuario}}"
                                   data-userid="{{$user->id}}">
                                    <i class="material-icons text-primary bicon">edit</i>
                                </a>
                                <a href="#" id="delete-user" data-username="{{$user->no_usuario}}" data-userid="{{$user->id}}">
                                    <i class="material-icons text-danger bicon">delete</i>
                                </a>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="6" class="alert alert-danger">Nenhum usuário cadastrado.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div>Exibindo {{$users->firstItem()}} - {{$users->lastItem()}} de {{$users->total()}} {{$users->total()>1?'Usuários':'Usuário'}}</div>
        <div class="d-flex justify-content-center">{{ $users->appends(request()->query())->fragment('usuarios')->links() }}</div>
    </div>
    <!-- Modal -->
    <div class="modal" id="user-details">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Detalhes do usuário - <span></span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="loading"></div>
                    <form id="form-user-details" method="post"></form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <div class="col-md-2 ">
                        <button id="alterar-usuario" type="button" class="btn btn-success btn-block">Alterar</button>
                    </div>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>

            </div>
        </div>
    </div>
@endsection
