<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:i:s");

        DB::table("estados")->insert([
            [
                "id"         => 11,
                "no_estado"       => "Rondônia",
                "uf"       => "RO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 12,
                "no_estado"       => "Acre",
                "uf"       => "AC",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 13,
                "no_estado"       => "Amazonas",
                "uf"       => "AM",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 14,
                "no_estado"       => "Roraima",
                "uf"       => "RR",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 15,
                "no_estado"       => "Pará",
                "uf"       => "PA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 16,
                "no_estado"       => "Amapá",
                "uf"       => "AP",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 17,
                "no_estado"       => "Tocantins",
                "uf"       => "TO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 21,
                "no_estado"       => "Maranhão",
                "uf"       => "MA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 22,
                "no_estado"       => "Piauí",
                "uf"       => "PI",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 23,
                "no_estado"       => "Ceará",
                "uf"       => "CE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 24,
                "no_estado"       => "Rio Grande do Norte",
                "uf"       => "RN",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 25,
                "no_estado"       => "Paraíba",
                "uf"       => "PB",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 26,
                "no_estado"       => "Pernambuco",
                "uf"       => "PE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 27,
                "no_estado"       => "Alagoas",
                "uf"       => "AL",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 28,
                "no_estado"       => "Sergipe",
                "uf"       => "SE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 29,
                "no_estado"       => "Bahia",
                "uf"       => "BA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 31,
                "no_estado"       => "Minas Gerais",
                "uf"       => "MG",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 32,
                "no_estado"       => "Espírito Santo",
                "uf"       => "ES",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 33,
                "no_estado"       => "Rio de Janeiro",
                "uf"       => "RJ",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 35,
                "no_estado"       => "São Paulo",
                "uf"       => "SP",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 41,
                "no_estado"       => "Paraná",
                "uf"       => "PR",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 42,
                "no_estado"       => "Santa Catarina",
                "uf"       => "SC",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 43,
                "no_estado"       => "Rio Grande do Sul",
                "uf"       => "RS",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 50,
                "no_estado"       => "Mato Grosso do Sul",
                "uf"       => "MS",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 51,
                "no_estado"       => "Mato Grosso",
                "uf"       => "MT",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 52,
                "no_estado"       => "Goiás",
                "uf"       => "GO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id"         => 53,
                "no_estado"       => "Distrito Federal",
                "uf"       => "DF",
                "created_at" => $now,
                "updated_at" => $now,
            ],
        ]);
    }
}
