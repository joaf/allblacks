<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('users');
});
Route::post('cities','StateController@getCities');

Route::prefix('users')->group(function (){
    Route::get('/', 'UserController@users');
    Route::get('new', 'UserController@new');
    Route::post('create','UserController@create');
    Route::post('update','UserController@update');
    Route::post('delete','UserController@delete');
    Route::post('details','UserController@details');
    Route::post('delete-all','UserController@deleteAll');
});


/*Route::post('importar', 'IndexController@importarXml');*/
Route::post('xml', 'IndexController@enviarXml');


Route::get('email', function(){
    return view('email');
});

Route::any('{url}',
    function() { return redirect('/'); })
    ->where('url', '.*');

