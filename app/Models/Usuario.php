<?php

namespace Project\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public function address()
    {
        return $this->hasMany(Endereco::class);
    }

    public function getUsers($filterData=[])
    {
        $users = $this->select('usuarios.*')->join('enderecos','enderecos.usuario_id','=','usuarios.id')
            ->join('cidades','cidades.id','=','enderecos.cidade_id')
            ->join('estados','estados.id','=','cidades.estado_id');

        //Se houver busca pelo filtro
        if (count($filterData) > 0) {
            if (isset($filterData['no_usuario'])) {
                $users = $users->where('no_usuario', 'like',  '%' . $filterData['no_usuario'] . '%');
            }
            if (isset($filterData['nu_documento'])) {
                $users = $users->where('usuarios.nu_documento',$filterData['nu_documento']);
            }
            if (isset($filterData['email'])) {
                $users = $users->where('usuarios.email',$filterData['email']);
            }
            if (isset($filterData['uf'])) {
                $users = $users->where('estados.id',$filterData['uf']);
            }
            if (isset($filterData['cidade_id'])) {
                $users = $users->where('cidades.id',$filterData['cidade_id']);
            }
        }

        return $users->orderBy('usuarios.created_at', 'desc')->paginate(10);
    }

    public function verifyEmail($email)
    {
        return $this->where('email',$email)->exists();
    }
}

