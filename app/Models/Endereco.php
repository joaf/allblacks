<?php

namespace Project\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    public function city()
    {
        return $this->belongsTo(Cidade::class,'cidade_id');
    }

   /* public function state()
    {
        return $this->belongsTo(Estado::class,'estado_id');
    }*/
}
