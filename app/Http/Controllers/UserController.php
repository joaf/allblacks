<?php

namespace Project\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Project\Models\Endereco;
use Project\Models\Estado;
use Project\Models\Usuario;

class UserController extends Controller
{
    private $states = [];

    public function __construct(Estado $states)
    {
        $this->states = $states::orderBy('uf')->get(['id','uf']);
    }

    public function users(Request $request, Usuario $users)
    {
        $users = $users->getUsers($request->except('page'));
        $states = $this->states;

        return view('usuarios',compact('users','states'));
    }

    public function new()
    {
        $states  = $this->states;

        return view('cadastro-usuarios',compact('states'));

    }

    public function details(Request $request, Usuario $user)
    {
        $user = $user->find($request->user_id);
        $states  = $this->states;

        return view('user-details',compact('user','states'));
    }

    public function create(Request $request, Usuario $user)
    {
        $email = $user->where('email',$request->email)->get();

        if(!count($email)){
            DB::beginTransaction();

            try {

                $user = new Usuario();

                $user->no_usuario = $request->nome;
                $user->nu_documento = $request->documento;
                $user->nu_telefone  = $request->telefone;
                $user->email     = $request->email;
                $user->save();

                $address = new Endereco();

                $address->no_endereco  = $request->endereco;
                $address->no_bairro    = $request->bairro;
                $address->nu_cep       = $request->cep;
                $address->cidade_id    = $request->city_id;
                $address->usuario_id   = $user->id;
                $address->save();

            } catch (\Exception $e) {
                DB::rollBack();

                return response()->json([
                    'status' => 'error',
                    'msg'    => $e->getMessage()]);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'msg'    => 'Usuário cadastrado com sucesso!']);
        } else {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Usuário já cadastrado!']);
        }
    }

    public function update(Request $request, Usuario $user)
    {
        if ($request->user_id > 0) {
            $user = $user->find($request->user_id);

            try{
                $user->no_usuario   = $request->nome;
                $user->nu_documento = $request->documento;
                $user->nu_telefone  = $request->telefone;
                $user->email        = $request->email;
                $user->save();

                $address = $user->address->first();
                $address->no_endereco   = $request->no_endereco;
                $address->no_bairro     = $request->no_bairro;
                $address->nu_cep        = $request->nu_cep;
                $address->cidade_id     = $request->city_id;
                $address->save();

                return response()->json([
                    'status' => 'success',
                    'msg'    => 'Os dados do usuário foram atualizados com sucesso!']);

            }catch (\Exception $e){
                return response()->json([
                    'status' => 'error',
                    'msg'    => $e->getMessage()]);
            }
        }
        return response()->json([
            'status' => 'error',
            'msg'    => 'Não foi possível localizar o usuário']);
    }

    public function delete(Request $request)
    {
        if (Usuario::destroy($request->user_id)) {
            return response()->json([
                'status' => 'success',
                'msg'    => 'Usuário removido com sucesso!',
                'reload' => true
            ]);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'Algo deu errado, por favor tente novamente.',
            'reload' => false
        ]);
    }

    //Remove todos os dados das tabelas(para testes)
    public function deleteAll()
    {
        if(count(Usuario::all()) > 0){

            try {
                DB::beginTransaction();
                DB::table('usuarios')->delete();
            }
            catch (\Exception $e){
                DB::rollBack();

                return response()->json([
                    'status' => 'error',
                    'msg'    => $e->getMessage()
                ]);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'msg'    => 'Arquivos apagados com sucesso!'
            ]);

        }
        return response()->json([
            'status' => 'error',
            'msg'    => 'Não foi possível realizar a operação.'
        ]);
    }
}
