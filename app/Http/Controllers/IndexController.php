<?php

namespace Project\Http\Controllers;


use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Project\Models\Cidade;
use Project\Models\Estado;
use Project\Models\Endereco;
use Project\Models\Usuario;


class IndexController extends Controller
{
    public function __construct(Estado $estado)
    {
        $this->estados = $estado::orderBy('uf')->get(['id','uf']);
    }

    /*public function usuarios(Request $request, Usuario $users)
    {
        $users = $users->getUsuarios($request->except('page'));
        $ufs = $this->ufs;

        return view('usuarios',compact('users','ufs'));
    }*/

    /*public function userDetails(Request $request, Usuario $user)
    {
        $user = $user->find($request->user_id);
        $estados  = $this->estados;

        return view('user-details',compact('user','estados'));
    }*/

    /*public function novoCadastro()
    {
        $estados  = $this->estados;

        return view('cadastro-usuarios',compact('estados'));
    }
    */
    /*public function inserirUsuario(Request $request, Usuario $usuario)
    {
        $email = $usuario->where('email',$request->email)->get();

        if(!count($email)){
            DB::beginTransaction();

            try {

                $usuario = new Usuario();

                $usuario->no_usuario = $request->nome;
                $usuario->nu_documento = $request->documento;
                $usuario->nu_telefone  = $request->telefone;
                $usuario->email     = $request->email;
                $usuario->save();

                $endereco = new Endereco();

                $endereco->no_endereco  = $request->endereco;
                $endereco->no_bairro    = $request->bairro;
                $endereco->nu_cep       = $request->cep;
                $endereco->cidade_id    = $request->cidade_id;
                $endereco->usuario_id   = $usuario->id;
                $endereco->save();

            } catch (\Exception $e) {
                DB::rollBack();

                return response()->json([
                    'status' => 'error',
                    'msg'    => $e->getMessage()]);
            }

            DB::commit();

            return response()->json([
                'status' => 'success',
                'msg'    => 'Usuário cadastrado com sucesso!']);
        } else {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Usuário já cadastrado!']);
        }
    }*/

    public function enviarXml(Request $request, Usuario $users, Endereco $address, Cidade $cities, Estado $states)
    {

        if (isset($_FILES['file']) && $_FILES['file']['error'] == 0) {
            $extensao = pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);

            /*$xml = File::get($_FILES['file']['tmp_name']);

            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->load($xml);

            dd($dom->getElementsByTagName('@attributes'));
            $properties = $dom->getElementsByTagName('property');*/

            if($extensao == 'xml'){
                //upload xml
                $xml = File::get($_FILES['file']['tmp_name']);
                $xmldata = simplexml_load_string($xml) or die("Não foi possível carregar o arquivo!.");

                  /*nome" => "Sra. Elizabeth Flores Franco Filho"
                  "documento" => "873.335.385-91"
                  "cep" => "52927-611"
                  "endereco" => "Av. Quintana, 59571. Bloco C"
                  "bairro" => "Evandro do Sul"
                  "cidade" => "So Irene"
                  "uf" => "PR"
                  "telefone" => "(41) 3529-4394"
                  "email" => "dserna@terra.com.br"
                  "ativo" => "1"*/

                //converte para array
                $xmlarray = json_decode(json_encode($xmldata), TRUE);

                $torcedores = $xmlarray['torcedor'];

                DB::beginTransaction();
                //Insere os dados no banco
                foreach ($torcedores as $torcedor){
                    try{
                        //verifica se existe email
                        if($users->verifyEmail($users->email = $torcedor['@attributes']['email'])){

                            //atualiza o usuario
                            $user  = $users->where('email',$torcedor['@attributes']['email'])->first();
                            $address = $user->address->first();
                        }else{
                            //cria uma nova instancia
                            $user  = new Usuario();
                            $address = new Endereco();
                        }

                        //Insere os dados
                        $user->no_usuario      = $torcedor['@attributes']['nome'];
                        $user->nu_documento = preg_replace('/[.-]/', '',$torcedor['@attributes']['documento']);
                        $user->nu_telefone  = $torcedor['@attributes']['telefone'];
                        $user->email     = $torcedor['@attributes']['email'];
                        $user->in_ativo     = $torcedor['@attributes']['ativo'] ?: 1;
                        $user->save();

                        $state_id = $states->where('uf',$torcedor['@attributes']['uf'])->get('id');
                        $randomCity = $cities->where('estado_id',$state_id[0]->id)->inRandomOrder()->first();

                        $address->no_endereco  = $torcedor['@attributes']['endereco'];
                        $address->no_bairro    = $torcedor['@attributes']['bairro'];
                        $address->nu_cep       = $torcedor['@attributes']['cep'];
                        $address->cidade_id    = $randomCity->id;
                        $address->usuario_id   = $user->id;
                        $address->save();

                    }catch (\Exception $e){
                        DB::rollBack();
                        return response()->json([
                            'status' => 'error',
                            'msg'    => $e->getMessage()]);
                    }
                }
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'msg'    => 'Dados cadastrados com sucesso!']);
            }
        }

        return response()->json([
            'status' => 'erro',
            'msg'    => 'Não foi possível ler ou carregar o arquivo!'
        ]);
    }
}
