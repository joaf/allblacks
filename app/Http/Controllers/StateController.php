<?php

namespace Project\Http\Controllers;

use Illuminate\Http\Request;
use Project\Models\Estado;

class StateController extends Controller
{
    public function getCities(Request $request, Estado $states)
    {
        $cities = $states->find($request->uf)->cities;

        return response()->json([
            'status' => 'sucesso',
            'msg'    => 'Consulta realizada com sucesso!',
            'cities'    => $cities]);
    }
}
