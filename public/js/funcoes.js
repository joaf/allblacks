$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.telefone').mask('(99)9999-9999');

    if($('table#usuarios tbody td.alert').length == 0){
        $('button#removerDados').prop('disabled', false);
    }

    $('form#form-filtro').on('reset',function (e) {
        window.location.replace('users');
    });

    $('#user-details').on('show.bs.modal', function (e) {
        var modal = $(this);
        var user_name = $(e.relatedTarget).data('username');
        var user_id   = $(e.relatedTarget).data('userid');

        $(this).find('.modal-title span').html(user_name);

        if (user_id > 0) {
            $.ajax({
                type: "POST",
                url: "users/details",
                data:{'user_id':user_id},
                beforeSend: function () {
                    modal.find('div.loading').show();
                },
                success: function (response) {
                    modal.find('div.loading').hide();
                    modal.find('div.modal-body form').html(response);
                },
                error: function (response) {

                }
            });
        }
    });

    $('div#user-details, form#form-cadastro, form#form-filtro').on('change','select[name=uf]',function(e) {
        var form = $(this).closest('form');
        var uf = $(this).val();

        if($(this).val() != ''){
            $.ajax({
                type: "POST",
                url: '/cities',
                data: {'uf':uf},
                beforeSend: function(){
                    form.find('select[name=city_id] option').html('Carregando...');
                },
                success: function (response) {
                    form.find('select[name=city_id]').html('');

                    if (form.attr('id') == 'form-filtro') {
                        form.find('select[name=city_id]').append('<option value="">Todas</option>');
                    }

                    $.each(response.cities, function(index,city){
                        console.log(response.msg);

                        var option = '<option value="'+city.id+'">'+city.no_cidade+'</option>';

                        form.find('select[name=city_id]').prop('disabled',false).append(option);
                    });
                },
                error: function (response) {
                    console.log(response.msg);
                }
            });
        }
    });

    $('div#user-details').on('click','button#alterar-usuario',function(e){
        e.preventDefault();
        var form = $('form#form-user-details');

            $.ajax({
                type: "POST",
                url: "users/update",
                data: form.serialize(),
                beforeSend: function (e) {
                    form.find('div.loading').show();
                },
                success: function (response) {
                    form.find('div.loading').hide();

                    switch (response.status) {
                        case 'error':
                            Swal.fire(
                                'Erro!',
                                response.msg,
                                'error'
                            );
                            break;
                        case 'success':
                            Swal.fire(
                                'Sucesso!',
                                response.msg,
                                'success'
                            ).then(function (e) {
                                location.reload();
                            });
                    }
                },
                error: function (xhr,response) {
                    form.find('div.loading').hide();

                }
            });
    });

    $('form#form-cadastro').on('submit', function (e) {
        e.preventDefault();

        var form = $(this);
        var input_vazio = false;

        //Verifica se há campos não preenchidos
        form.find('select,input').each(function () {

            $(this).removeClass('obrigatorio');

            if($(this).val() == ''){
                input_vazio = true;
                $(this).addClass('obrigatorio');
            }
        });

        if (input_vazio) {
            form.find('div.alert-danger').slideDown();
        } else {
            form.find('div.alert-danger').slideUp();

            $.ajax({
                type: "POST",
                url: '/users/create',
                data: form.serialize(),
                beforeSend: function (){
                    form.find('div.loading').show();
                },
                success: function (response) {
                    form.find('div.loading').hide();
                    switch (response.status){
                        case 'success':
                            Swal.fire(
                                'Sucesso!',
                                response.msg,
                                'success'
                            ).then(form.trigger('reset'));
                            break;
                        case 'error':
                            Swal.fire(
                                'Ops!',
                                response.msg,
                                'warning'
                            ).then(function () {
                                form.find('input[name=email]').val('');
                            });
                            break;
                    }
                },
                error: function (response) {
                    Swal.fire(
                        'Erro!',
                        response.msg,
                        'error'
                    );
                }
            });
        }
    });

    $('table#usuarios a#delete-user').on('click',function (e) {
        e.preventDefault();
        var user_name = $(this).data('username');
        var user_id   = $(this).data('userid');

        if(user_id > 0){
            Swal.fire({
                title: 'Remover usuário?',
                text: 'Tem certeza que deseja remover o usuário '+user_name+' ?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não'
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "users/delete",
                        data: {'user_id':user_id},
                        beforeSend: function (){
                           $('div.loading').show();
                        },
                        success: function (response) {
                           $('div.loading').hide();
                            switch (response.status) {
                                case 'error':
                                    Swal.fire(
                                        'Erro!',
                                        response.msg,
                                        'error'
                                    );
                                    break;
                                case 'success':
                                    Swal.fire(
                                        'Sucesso!',
                                        response.msg,
                                        'success'
                                    ).then(function (e) {
                                        location.reload();
                                    });
                            }
                        },
                        error: function (response) {
                            $('div.loading').hide();
                            console.log(retorno.msg);
                        }
                    });
                }
            });
        }
    });

    $('button#removerDados').on('click',function(e){

        Swal.fire({
            title: 'Tem certeza?',
            text: "Todos os registros serão apagados!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "users/delete-all",
                    beforeSend: function (){
                        $('div.loading').show();
                    },
                    success: function (retorno) {
                        $('div.loading').hide();
                        switch (retorno.status) {
                            case 'error':
                                Swal.fire(
                                    'Erro!',
                                    retorno.msg,
                                    'error'
                                );
                                break;
                            case 'success':
                                Swal.fire(
                                    'Sucesso!',
                                    retorno.msg,
                                    'success'
                                ).then(function (e) {
                                    location.reload();
                                });
                        }
                    },
                    error: function (retorno) {
                        $('div.loading').hide();
                        console.log(retorno.msg);
                    }
                });
            }
        });
    });

    $('#upload').change(function () {
        var form = $('form#xml');
        var file = $('#upload')[0].files[0].name;

        form.find('span.custom-file-upload').text(file);
        form.find('label.btn-success').removeClass('disabled');
        form.find('input#submitXml').prop('disabled', false);
    });

    $('input#submitXml').on('click', function (e) {
        e.preventDefault();

        var form = $('form#xml');
        var data = new FormData(form.get(0));

        $.ajax({
            url: 'xml',
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            beforeSend: function () {
                form.find('div.loading').show();
            },
            success: function (retorno) {
                form.find('div.loading').hide();
                switch (retorno.status) {
                    case 'error':
                        Swal.fire('Erro!',retorno.msg,'error');
                        break;
                    case 'success':
                        Swal.fire('Sucesso!',retorno.msg,'success'
                        ).then(function (e) {
                            $('form#form-filtro').trigger('reset');
                        });
                }
            },
            error: function (xhr,retorno) {
                form.find('div.loading').hide();
                Swal.fire('Erro!','Algo deu errado! Por favor, tente novamente.','error');
                console.log(xhr.responseText);
            }
        });

       /* $.ajax({
            type: "POST",
            url: 'xml',
            data: form.serialize(),
            beforeSend: function () {
                form.find('button').prop('disabled', true);
                form.find('.loading').show();
            },
            success: function (retorno) {
                if (retorno.status == 'success') {
                    $('div#contact-info').find('div#sendmessage').slideDown();
                    form.find('.loading').hide();
                    form.find('button').prop('disabled', false);

                    form.trigger('reset');

                    setTimeout(function () {
                        $('div#contact-info').find('div#sendmessage').slideUp();
                    }, 2000)

                } else {
                    $('div#contact-info').find('div#errormessage').slideDown().html('Algo deu errado! Por favor, tente novamente.');
                    form.find('.loading').hide();

                    setTimeout(function () {
                        $('div#contact-info').find('div#errormessage').slideUp();
                        location.reload();
                    }, 2500)
                }
            }
        });*/
    });
});
